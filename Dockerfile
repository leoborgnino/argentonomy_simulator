#Download base image ubuntu 20.04
FROM ubuntu:20.04

# LABEL about the custom image
LABEL maintainer="leandro.borgnino@mi.unc.edu.ar"
LABEL version="0.1"
LABEL description="This image is built for execute argentonomy simulator"

# WORKDIR
RUN mkdir /home/argentonomy/
WORKDIR /home/argentonomy/

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Update Ubuntu Software repository
RUN apt update

# Prerequisitos

RUN apt install -y software-properties-common
RUN add-apt-repository universe


RUN apt update && apt install -y curl gnupg lsb-release
RUN curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg


RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/ros2.list > /dev/null

# Instalar tools necesarias

RUN apt update && apt install -y \
  build-essential \
  cmake \
  git \
  python3-colcon-common-extensions \
  python3-flake8 \
  python3-pip \
  python3-pytest-cov \
  python3-rosdep \
  python3-setuptools \
  python3-vcstool \
  wget
  
# install some pip packages needed for testing
RUN python3 -m pip install -U \
  flake8-blind-except \
  flake8-builtins \
  flake8-class-newline \
  flake8-comprehensions \
  flake8-deprecated \
  flake8-docstrings \
  flake8-import-order \
  flake8-quotes \
  pytest-repeat \
  pytest-rerunfailures \
  pytest \
  setuptools

RUN apt install -y libtinyxml2-dev
RUN apt install -y ros-galactic-cv-bridge
RUN apt install -y python3-image-geometry
RUN apt-get install -y libqt5widgets5
RUN apt-get install -y libassimp-dev
# Descarga ROS2
RUN wget --no-check-certificate --content-disposition https://github.com/ros2/ros2/releases/download/release-galactic-20210716/ros2-galactic-20210616-linux-focal-amd64.tar.bz2
RUN mkdir -p /home/argentonomy/ros2_galactic
RUN tar xfv /home/argentonomy/ros2-galactic-20210616-linux-focal-amd64.tar.bz2 -C /home/argentonomy/ros2_galactic

# Instalar dependencias ros2

WORKDIR /home/argentonomy/ros2_galactic/ros2-linux/
RUN rosdep init
RUN rosdep update

RUN rosdep install --from-paths /home/argentonomy/ros2_galactic/ros2-linux/src --ignore-src -y --skip-keys "fastcdr rti-connext-dds-5.3.1 urdfdom_headers"

# Descargar paquetes extra
RUN apt install -y libtinyxml2.6.2v5
RUN apt install -y ros-galactic-ament-cmake-python
RUN apt install -y libssl-dev
RUN apt install -y python3-opencv
RUN apt install -y libboost-python-dev
RUN apt install -y python3-tqdm
RUN apt install -y python3-pykdl
RUN pip3 install transforms3d
RUN pip3 install torch
RUN pip3 install pykitti
RUN pip3 install lark
RUN pip3 install opencv-contrib-python
RUN pip3 install tensorflow
RUN pip3 install torchvision
RUN pip3 install seaborn
RUN pip3 install easydict
RUN pip3 install scipy
RUN pip3 install shapely

# Bajar REPO
#WORKDIR /home/argentonomy/
#RUN git clone https://gitlab.com/leoborgnino/argentonomy_simulator.git

# Variables de entorno
RUN mkdir -p /home/argentonomy/argentonomy_simulator/
WORKDIR /home/argentonomy/argentonomy_simulator/
RUN echo "source /opt/ros/galactic/setup.bash > /dev/null 2>&1" >> ~/.bashrc
RUN echo "source /home/argentonomy/ros2_galactic/ros2-linux/setup.bash > /dev/null 2>&1" >> ~/.bashrc
RUN echo "source /home/argentonomy/argentonomy_simulator/ros/install/setup.bash > /dev/null 2>&1" >> ~/.bashrc
RUN echo "export ROS_ROOT_PATH=/home/argentonomy/argentonomy_simulator/ros" >> ~/.bashrc

# KITTI dataset raw
RUN mkdir -p /home/argentonomy/datasets/KITTI/raw/2011_09_26/
ADD datasets/KITTI/raw/2011_09_26/ /home/argentonomy/datasets/KITTI/raw/2011_09_26/
#ADD datasets/KITTI/sequences/00/ /home/argentonomy/datasets/KITTI/sequences/00/
#ADD datasets/KITTI/poses/ /home/argentonomy/datasets/KITTI/poses/

# VNC
RUN apt-get update
RUN apt-get install -y x11vnc xvfb 
RUN mkdir ~/.vnc
RUN x11vnc -storepasswd 1234 ~/.vnc/passwd
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
EXPOSE 5900

ENTRYPOINT ["/entrypoint.sh"]
