[![pipeline status](https://gitlab.com/leoborgnino/ffulgor_simulator/badges/master/pipeline.svg)](https://gitlab.com/leoborgnino/ffulgor_simulator/-/commits/master)

## ffulgor: Simulador de algoritmos de Navegación Autónoma implementado en ROS2 por Fundación Fulgor

> ffulgor: Autonomous Navigation Algorithm Simulator implemented on ROS2 by Fundación Fulgor

Este proyecto de simulador fue comenzado para generar un entorno donde diferentes proyectos o grupo de trabajo puedan testear sus algoritmos de navegación autónoma e integrarlos con otros ya desarrollados de forma sencilla. Además, proveer sistemas de verificación y visualización que permitan una correcta evaluación de los mismos.

> This project intention is to generate an environment where different projects or teams can test autonomous navigation algorithms along to another algorithms prevoiusly developed. In addition, the simulator is able to provide visualization and test methods.

## Video ejemplo

![video-ejemplo](images/video_ejemplo2.mp4)  


## Instalación, estructura, tutoriales de uso

Para más información sobre el simulador: cómo instalarlo, cómo usarlo, la estructura del mismo:
> More information about this simulator: installation, structure, tutorials:

[Wiki ffulgor](https://gitlab.com/leoborgnino/ffulgor_simulator/-/wikis/home)
