import os
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():
    ld = LaunchDescription()

    config = os.path.join(
        get_package_share_directory('ffulgor_bringup'),
        'config',
        'default.yaml'
        )
    
    dataloader = Node(
            package='ffulgor_dataloader',
            name='DataLoaderNode',
            executable='dataloader',
            output='screen',
            emulate_tty=True,
            arguments=[('__log_level:=debug')],
            parameters = [config])
    
    visualodom = Node(
            package='ffulgor_visualodom',
            name='VisualOdomNode',
            executable='visualodom',
            output='screen',
            emulate_tty=True,
            arguments=[('__log_level:=debug')],
            parameters = [config])

    ld.add_action(dataloader)
    ld.add_action(visualodom)

    return ld
