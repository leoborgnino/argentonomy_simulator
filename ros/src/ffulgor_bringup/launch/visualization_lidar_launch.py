import os
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():
    ld = LaunchDescription()

    # URDF KITTI
    urdf_file_name = 'prius.urdf'
    urdf = os.path.join(
        get_package_share_directory('ffulgor_bringup'),
        'urdf',
        urdf_file_name)
    with open(urdf, 'r') as infp:
        robot_desc = infp.read()

    config = os.path.join(
        get_package_share_directory('ffulgor_bringup'),
        'config',
        'default_lidar.yaml'
        )

    # Rviz2 Path config
    config_rviz2 = os.path.join(
        get_package_share_directory('ffulgor_bringup'),
        'config',
        'lidar_raw.rviz'
    )

    # Rviz2 node
    rviz2_node = Node(
        package='rviz2',
        namespace='lidar_kitti_raw',
        executable='rviz2',
        name='lidar_kitti_raw_rviz2',
        output='screen',
        arguments=[["-d"], [config_rviz2]],
    )


    state_publisher = Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            parameters=[{'robot_description': robot_desc}],
            arguments=[urdf])

    
    dataloader = Node(
            package='ffulgor_dataloader',
            name='DataLoaderNode',
            executable='dataloader',
            output='screen',
            emulate_tty=True,
            arguments=[('__log_level:=debug')],
            parameters = [config])
    
    visualodom = Node(
            package='ffulgor_visualodom',
            name='VisualOdomNode',
            executable='visualodom',
            output='screen',
            emulate_tty=True,
            arguments=[('__log_level:=debug')],
            parameters = [config])

    visualization = Node(
            package='ffulgor_visualization',
            name='VisualizationNode',
            executable='visualization',
            output='screen',
            emulate_tty=True,
            arguments=[('__log_level:=debug')],
            parameters = [config])

    ld.add_action(state_publisher)
    ld.add_action(visualization)
    ld.add_action(dataloader)
    ld.add_action(visualodom)
    ld.add_action(rviz2_node)

    return ld
