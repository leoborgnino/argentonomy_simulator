import os
import sys
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory

try:
    if ( sys.argv[4] == "disable_vis:=True"): # Cambiar a Variables de Launch ROS2
        disable_visualization = True
    else:
        disable_visualization = False
except:
    disable_visualization = False

def generate_launch_description():
    ld = LaunchDescription()

    config = os.path.join(
        get_package_share_directory('ffulgor_bringup'),
        'config',
        'default_lidar_detection.yaml'
        )
    
    # Rviz2 Path config
    config_rviz2 = os.path.join(
        get_package_share_directory('ffulgor_bringup'),
        'config',
        'lidar_detection_visualization.rviz'
    )
    
    dataloader = Node(
            package='ffulgor_dataloader',
            name='DataLoaderNode',
            executable='dataloader',
            output='screen',
            emulate_tty=True,
            arguments=[('__log_level:=debug')],
            parameters = [config])

    od_lidar = Node(
            package='ffulgor_od_lidar',
            name='OdLidarNode',
            executable='od_lidar',
            output='screen',
            emulate_tty=True,
            arguments=[('__log_level:=debug')],
            parameters = [config])
    
    # Rviz2 node
    rviz2_node = Node(
            package='rviz2',
            namespace='lidar_detection',
            executable='rviz2',
            name='lidar_detection_rviz2',
            output='screen',
            arguments=[["-d"], [config_rviz2]])
    
    visualization = Node(
            package='ffulgor_visualization',
            name='VisualizationNode',
            executable='visualization',
            output='screen',
            emulate_tty=True,
            arguments=[('__log_level:=debug')],
            parameters = [config])

    ld.add_action(visualization)
    ld.add_action(dataloader)
    ld.add_action(od_lidar)
    if (disable_visualization == False):
        ld.add_action(rviz2_node)

    return ld
