import os
import sys
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory

try:
    if ( sys.argv[4] == "disable_vis:=True"): # Cambiar a Variables de Launch ROS2
        disable_visualization = True
    else:
        disable_visualization = False
except:
    disable_visualization = False

def generate_launch_description():
    ld = LaunchDescription()

    config = os.path.join(
        get_package_share_directory('ffulgor_bringup'),
        'config',
        'ts_detection.yaml'
        )

    # Rviz2 Path config
    config_rviz2 = os.path.join(
        get_package_share_directory('ffulgor_bringup'),
        'config',
        'ts_recog.rviz'
    )
    
    # Rviz2 node
    rviz2_node = Node(
        package='rviz2',
        namespace='ts_recog',
        executable='rviz2',
        name='ts_recog_rviz2',
        output='screen',
        arguments=[["-d"], [config_rviz2]],
    )
    
    dataloader = Node(
            package='ffulgor_dataloader',
            name='DataLoaderNode',
            executable='dataloader',
            output='screen',
            emulate_tty=True,
            arguments=[('__log_level:=debug')],
            parameters = [config])

    ts_recog = Node(
            package='ffulgor_ts_recog',
            name='Traffic_Sign_node',
            executable='ts_recog',
            output='screen',
            emulate_tty=True,
            arguments=[('__log_level:=debug')],
            parameters = [config])

    ld.add_action(dataloader)
    ld.add_action(ts_recog)
    if (disable_visualization == False):
        ld.add_action(rviz2_node)

    return ld

