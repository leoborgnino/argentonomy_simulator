import sys
import os
from easydict import EasyDict as edict
import torch
import numpy as np

from ffulgor_msgs.srv import LidarLoader
from ffulgor_msgs.msg import DetectionAndRaw
import sensor_msgs_py.point_cloud2 as pc2
from sensor_msgs.msg import PointCloud2
from std_msgs.msg import Float64MultiArray, MultiArrayDimension, MultiArrayLayout

from transforms3d.quaternions import axangle2quat

import rclpy
from rclpy.node import Node

import cv2

sys.path.append(os.path.expandvars('$ROS_ROOT_PATH/src/ffulgor_od_lidar/ffulgor_od_lidar'))

from config import kitti_config as cnf
from data_process import kitti_bev_utils
from models.model_utils import create_model
from utils.evaluation_utils import rescale_boxes, post_processing_v2
from utils.misc import time_synchronized

import warnings # FIXME
warnings.filterwarnings("ignore")

class OdLidarNode(Node):

    def __init__(self):
        super().__init__("OdLidarNode")
        self.get_logger().info("OdLidarNode Node Started")
        
        #### Lidar Client ####
        
        self.client_lidar_raw = self.create_client(LidarLoader, '/ffulgor/dataloader/lidar_raw') #Crea cliente
        
        while not self.client_lidar_raw.wait_for_service(timeout_sec=1.0): #Espera conexion con el servidor
            self.get_logger().info('service not available, waiting again...')
            
        self.lidar_req = LidarLoader.Request() 

        self.sequence_idx = 1

        #### Publishers ####

        self.lidar_detection_pub = self.create_publisher(Float64MultiArray, '/ffulgor/od_lidar/lidar_detection',10)
        self.lidar_detection_raw = self.create_publisher(DetectionAndRaw, '/ffulgor/od_lidar/lidar_detection_and_raw',10)

        # YOLO parameters as NODE parameters
        self.declare_parameter('saved_fn', 'complexer_yolov4')
        self.declare_parameter('arch', 'darknet')
        self.declare_parameter('cfgfile', '$ROS_ROOT_PATH/src/ffulgor_od_lidar/ffulgor_od_lidar/config/cfg/complex_yolov4.cfg')
        self.declare_parameter('pretrained_path', '$ROS_ROOT_PATH/src/ffulgor_od_lidar/ffulgor_od_lidar/checkpoints/complex_yolov4/complex_yolov4_mse_loss.pth')
        self.declare_parameter('use_giou_loss', True)
        self.declare_parameter('no_cuda', False)
        self.declare_parameter('gpu_idx', 0)
        self.declare_parameter('img_size', 608)
        self.declare_parameter('conf_thresh', 0.5)
        self.declare_parameter('nms_thresh', 0.5)
        self.declare_parameter('save_image', True)

        # Config array for YOLO
        self.configs = [[parameter.name.split('/')[-1], parameter.value] for parameter in self._parameters.values()]
        self.configs = self.to_edict(self.configs)

        # Evaluate paths
        self.configs.cfgfile = os.path.expandvars(self.configs.cfgfile)
        self.configs.pretrained_path = os.path.expandvars(self.configs.pretrained_path)

        # Create YOLOv4 model
        self.model = create_model(self.configs)

        assert os.path.isfile(self.configs.pretrained_path), "No file at {}".format(self.configs.pretrained_path)
        
        # Set GPU
        self.configs.device = torch.device('cpu' if self.configs.no_cuda else 'cuda:{}'.format(self.configs.gpu_idx))
        self.model = self.model.to(device=self.configs.device)

        # Load weights
        self.model.load_state_dict(torch.load(self.configs.pretrained_path, map_location=self.configs.device))

        self.out_cap = None
        self.t1 = None
        self.t2 = None

        self.model.eval() # Set to evaluation mode
    
    def send_request_pc(self):
        self.lidar_req.sequence = self.sequence_idx
        self.lidar_future = self.client_lidar_raw.call_async(self.lidar_req)

    def PublishLidarDetection(self, msg):
        self.lidar_detection_pub.publish(msg)

    def PublishLidarDetRaw(self, msg):
        self.lidar_detection_raw.publish(msg)

    # Returns detections with shape: (x_center, y_center, width, length, imag, real, object_confidence, class_score, class_predicted)
    def inference(self, pc2_msg):
        detections = None
        with torch.no_grad():
            rgb_bev = self.pc2_to_bev(pc2_msg)
            rgb_bev = self.to_tensor(rgb_bev)
            input_img = rgb_bev.to(device=self.configs.device).float()
            self.t1 = time_synchronized()
            output = self.model(input_img)
            self.t2 = time_synchronized()
            detections = post_processing_v2(output, conf_thresh=self.configs.conf_thresh, nms_thresh=self.configs.nms_thresh)
            
            ## For BEV image generation
            rgb_bev_vis = []
            rgb_bev_vis = rgb_bev.squeeze() * 255
            rgb_bev_vis = rgb_bev_vis.permute(1, 2, 0).numpy().astype(np.uint8)
            rgb_bev_vis = cv2.resize(rgb_bev_vis, (self.configs.img_size, self.configs.img_size))
               
            # rgb_bev_nobb = rgb_bev_vis.copy() # BEV without the drawed bounding boxes
            # bb_coord = []
            
            if (detections != [None]):
                detections_vis = rescale_boxes(detections, self.configs.img_size, rgb_bev_vis.shape[:2])
                for x, y, w, l, im, re, *_, cls_pred in detections_vis:
                    yaw = np.arctan2(im, re)
                    # Draw rotated box
                    kitti_bev_utils.drawRotatedBox(rgb_bev_vis, x, y, w, l, yaw, cnf.colors[int(cls_pred)])
                    # # Get BB Coords in pxls on BEV
                    # bb_coord.append(kitti_bev_utils.get_corners(x, y, w, l, yaw))
                if self.configs.save_image:
                    cv2.imwrite('detections%d.png'%self.sequence_idx, rgb_bev_vis)
            
                detections = detections[0].tolist()
            
            # result = [detections, rgb_bev_vis, rgb_bev_nobb, bb_coord]
            result = [detections, rgb_bev_vis]
                
        return result
    
    def pc2_to_bev(self, pc2_msg):

        cloud = self.to_array2d(pc2.read_points_list(pc2_msg, ["x", "y", "z", "intensity"]))
        #cloud = kitti_bev_utils.removePoints(cloud, cnf.boundary)
        cloud = kitti_bev_utils.removePoints(cloud, cnf.ford_boundary)
        rgb_bev = kitti_bev_utils.makeBVFeature(cloud, cnf.DISCRETIZATION, cnf.boundary)

        return rgb_bev

    def to_array2d(self, lst):
        array2d = np.zeros((len(lst), 4))
        for a in range(len(lst)):
            array2d[a, 0] = lst[a].x
            array2d[a, 1] = lst[a].y
            array2d[a, 2] = lst[a].z
            array2d[a, 3] = lst[a].intensity
        return array2d  

    def to_edict(self, list):
        cfg_edict = edict()
        for u in range(len(list)):
            cfg_edict[list[u][0]] = list[u][1]
        return cfg_edict

    def to_tensor(self, array):
        temp_array = np.zeros((1, 3, 608, 608))
        temp_array[0, :, :, :] = array[:, :, :]
        tensor = torch.tensor(temp_array)
        return tensor
    
    #### Transformations from detections (bev) to point cloud (rviz) ####
    
    def tf_detections(self, detections):
        detections_tf = []
        
        for detection in detections:
            detect_tf = np.zeros(11)
            detect_tf[0], detect_tf[1], _ = self.coordinate_tf(detection[0], detection[1]) # x_center, y_center
            detect_tf[2], detect_tf[3] = self.size_tf(detection[2], detection[3]) # width, length
            
            angle = - np.pi - np.angle(np.complex(detection[5], detection[4]))
            quat = axangle2quat([1,0,0], angle, True) # Quaternion for rot angle
            detect_tf[4] = quat[0] 
            detect_tf[5] = quat[1]
            detect_tf[6] = quat[2]
            detect_tf[7] = quat[3]
            
            detect_tf[8] = detection[6] # Object Confidence
            detect_tf[9] = detection[7] # Class score
            detect_tf[10] = detection[8] # Class
            detections_tf.append(detect_tf)
        return detections_tf
    
    # Transform pose coordinates 
    def coordinate_tf(self, xb=None, yb=None, zb=None):
        xl = None 
        yl = None
        zl = None
        if (xb != None):
            xl = (50/608) * yb
        if (yb != None):
            yl = (50/608) * xb - 25
        if (zb != None):
            zl = (91/6375) * zb - 2.37
        return xl, yl, zl
    
    # Transform size of the box
    def size_tf(self, wb, lb):
        wl = (lb*50)/608
        ll = (wb*50)/608
        return wl, ll
    
    # # Get z coordinate
    # def get_z(self, rgb_bev_nobb, bb_coord):
    #     heightmap = rgb_bev_nobb[:, :, 1]
    #     tf_bbs = self.change_coord_format(bb_coord)
        
    #     height = heightmap.shape[0]
    #     width = heightmap.shape[1]
    #     mask = np.zeros((height, width), dtype=np.uint8)
    #     max_heights = []
        
    #     # Crop the region of bounding boxes and get the max height in each bb
    #     for bb in tf_bbs:
    #         cv2.fillPoly(mask, bb, (255))
    #         res = cv2.bitwise_and(heightmap, heightmap, mask = mask)
    #         rect = cv2.boundingRect(bb) # returns (x,y,w,h) of the rect
    #         max_pxl_height = np.amax(res[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]])
    #         _,_,zl = self.coordinate_tf(zb=max_pxl_height)
    #         max_heights.append(zl)     
        
        
    # Green for cars, blue for pedestrians, red for cyclist (r, g, b)
    def choose_color(self, class_predicted):
        colors = [[0.0, 1.0, 0.0], [0.0, 0.0, 1.0], [1.0, 0.0, 0.0]]
        return colors[class_predicted]
    
    def change_coord_format(self, bb_coord):
        tf_bbs = []
        for i, bb in enumerate(bb_coord):
            tf_bbs.append(np.around(np.array([[[bb[0, 0],bb[0, 1]],[bb[1, 0],bb[1, 1]],[bb[2, 0],bb[2, 1]],[bb[3, 0],bb[3, 1]]]])))
            tf_bbs[i] = tf_bbs[i].astype(int)
        return tf_bbs
    
    def create_detections_array(self, detections_pc):
        n_detections = detections_pc.__len__()
        
        detections_plain = []
        for detection in detections_pc:
            for data in detection:
                detections_plain.append(data)
                
        dim = [MultiArrayDimension(), MultiArrayDimension()]
        dim[0].label = 'detections'
        dim[0].size = n_detections
        dim[0].stride = n_detections*11
        dim[1].label = 'detections_data'
        dim[1].size = 11
        dim[1].stride = 11
        
        layout = MultiArrayLayout()
        layout.dim = dim
        msg = Float64MultiArray()
        msg.layout = layout
        msg.data = detections_plain
        return msg
        
def main():
    rclpy.init() #Inicializa libreria ros
    node = OdLidarNode() #Crea nodo lidar
    
    while rclpy.ok():
        node.send_request_pc()
        rclpy.spin_until_future_complete(node, node.lidar_future)
    
        try:
            torch.cuda.empty_cache() # To clean GPU memory
            pc2_cloud = node.lidar_future.result().lidar_data
        except Exception as e:
            node.get_logger().info(
                'Service call failed %r' % (e,))
        else:
            node.get_logger().info('Cloud obtained for BIN %d' % (node.sequence_idx))
            
            result = node.inference(pc2_cloud) # result = [detections, rgb_bev_vis]
            
            if result[0] != [None]:
                detections_pc_coord = node.tf_detections(result[0])
                detections_pc_msg = node.create_detections_array(detections_pc_coord) # Msg to be published to visualization node
                node.PublishLidarDetection(detections_pc_msg)
                msg_pc_det = DetectionAndRaw()
                msg_pc_det.point_cloud = pc2_cloud
                msg_pc_det.detections = detections_pc_msg
                node.PublishLidarDetRaw(msg_pc_det)
            else:
                msg_pc_det = DetectionAndRaw()
                msg_pc_det.point_cloud = pc2_cloud
                msg_pc_det.detections = Float64MultiArray()
                node.PublishLidarDetRaw(msg_pc_det)
            
            node.get_logger().info('\tDone inferencing the {}th sample, time: {:.1f}ms, speed {:.2f}FPS'.format(node.sequence_idx, (node.t2 - node.t1) * 1000,
                                                                                            1 / (node.t2 - node.t1)))
             
            if(node.sequence_idx < 446): node.sequence_idx += 1
            else: sys.exit("End of the program")
            
if __name__ == '__main__':
    main()
