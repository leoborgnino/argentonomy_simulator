from .FrameByFrameMatcher import FrameByFrameMatcher
from .SuperGlueMatcher import SuperGlueMatcher


def create_matcher(conf):
    try:
        code_line = f"{conf['matcher_name']}(conf)"
        matcher = eval(code_line)
    except NameError:
        raise NotImplementedError(f"{conf['matcher_name']} is not implemented yet.")

    return matcher
