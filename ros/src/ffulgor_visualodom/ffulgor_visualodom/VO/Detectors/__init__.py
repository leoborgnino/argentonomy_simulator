from .HandcraftDetector import HandcraftDetector
from .SuperPointDetector import SuperPointDetector


def create_detector(conf):
    try:
        code_line = f"{conf['detector_name']}(conf)"
        detector = eval(code_line)
    except NameError:
        raise NotImplementedError(f"{conf['detector_name']} is not implemented yet.")

    return detector
