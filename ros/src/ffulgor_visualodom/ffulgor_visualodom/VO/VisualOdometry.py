# based on: https://github.com/uoip/monoVO-python

import numpy as np
import cv2
import time
import sys
import os

from .Detectors import create_detector
from .Matchers import create_matcher

class VisualOdometry(object):
    """
    A simple frame by frame visual odometry
    """

    def __init__(self, K, param_list, time_profile):
        """
        :param K -> Matriz de 3x3 serializada de la camara
        :param cam: camera parameters
        """
        # feature detector and keypoints matcher
        self.detector = create_detector(param_list)
        self.matcher = create_matcher(param_list)

        # camera parameters
        self.focal = K[0]
        self.pp = (K[2], K[5])

        # frame index counter
        self.index = 0

        # keypoints and descriptors
        self.kptdescs = {}

        # pose of current frame
        self.cur_R = np.identity(3)
        self.cur_t = np.zeros((3, 1))
        self.R_tmp = np.identity(3)

        # configs
        self.time_profile = time_profile
        self.iteration = 0.
        self.time_detector = 0.
        self.time_matcher = 0.
        self.time_recover = 0.

    def set_initial_mat (self, R, t):
        self.cur_t = t
        self.cur_R = R

    def update(self, image, absolute_scale=1):
        """
        update a new image to visual odometry, and compute the pose
        :param image: input image
        :param absolute_scale: the absolute scale between current frame and last frame
        :return: R and t of current frame
        """
        self.iteration += 1
        if(self.time_profile == True):
            time_start_detector = time.time()
        kptdesc = self.detector(image)
        if(self.time_profile == True):
            time_end_detector = time.time()
            self.time_detector = (time_end_detector-time_start_detector)
            #print ("Time Detector %d: %f"%(self.iteration,self.time_detector))

        # first frame
        if self.index == 0:
            # save keypoints and descriptors
            self.kptdescs["cur"] = kptdesc

            # start point
        else:
            # update keypoints and descriptors
            self.kptdescs["cur"] = kptdesc

            # match keypoints
            if(self.time_profile == True):
                time_start_matcher = time.time()
            matches = self.matcher(self.kptdescs)
            if(self.time_profile == True):
                time_end_matcher = time.time()
                self.time_matcher = (time_end_matcher-time_start_matcher)
                #print ("Time Matcher %d: %f"%(self.iteration,self.time_matcher))

            # compute relative R,t between ref and cur frame
            if(self.time_profile == True):
                time_start_recover = time.time()
            E, mask = cv2.findEssentialMat(matches['cur_keypoints'], matches['ref_keypoints'],
                                           focal=self.focal, pp=self.pp,
                                           method=cv2.RANSAC, prob=0.999, threshold=1.0)
            try:
                _, R, t, mask = cv2.recoverPose(E, matches['cur_keypoints'], matches['ref_keypoints'], focal=self.focal, pp=self.pp)
                # get absolute pose based on absolute_scale
                if (absolute_scale > 0.1):
                    self.cur_t = (self.cur_t + absolute_scale * self.cur_R.dot(t))
                    #print(R)
                    self.R_tmp = R
                    self.cur_R = R.dot(self.cur_R)
            except:
                print("Frame Error")
                self.cur_t = self.cur_t
                self.cur_R = self.cur_R

            if(self.time_profile == True):
                time_end_recover = time.time()
                self.time_recover = (time_end_recover-time_start_recover)
                #print ("Time Recover %d: %f"%(self.iteration,self.time_recover))
            

        self.kptdescs["ref"] = self.kptdescs["cur"]

        self.index += 1
        return self.cur_R, self.cur_t


class AbsoluteScaleComputer(object):
    def __init__(self):
        self.prev_pose = None
        self.cur_pose = None
        self.count = 0

    def update(self, pose):
        self.cur_pose = pose

        scale = 1.0
        if self.count != 0:
            scale = np.sqrt(
                (self.cur_pose[0, 3] - self.prev_pose[0, 3]) * (self.cur_pose[0, 3] - self.prev_pose[0, 3])
                + (self.cur_pose[1, 3] - self.prev_pose[1, 3]) * (self.cur_pose[1, 3] - self.prev_pose[1, 3])
                + (self.cur_pose[2, 3] - self.prev_pose[2, 3]) * (self.cur_pose[2, 3] - self.prev_pose[2, 3]))

        self.count += 1
        self.prev_pose = self.cur_pose
        return scale
