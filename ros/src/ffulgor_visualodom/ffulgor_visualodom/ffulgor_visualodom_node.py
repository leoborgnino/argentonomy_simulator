#! /usr/bin/env python3

import os
import math
import rclpy
from rclpy.node import Node
from rclpy.duration import Duration
import matplotlib.pyplot as plt
import cv2
import numpy as np
import sys
import threading
import matplotlib.cm as cm

from std_msgs.msg import Float64
from std_msgs.msg import String

from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import Image
from sensor_msgs.msg import CameraInfo
from geometry_msgs.msg import TransformStamped

from ffulgor_msgs.srv import ImageLoader
from ffulgor_msgs.srv import CameraInfoLoader
from ffulgor_msgs.srv import PoseLoader

from ffulgor_msgs.msg import KeyPoints, KeyPoint

sys.path.append(os.path.expandvars('$ROS_ROOT_PATH/src/ffulgor_visualodom/ffulgor_visualodom'))

from VO.VisualOdometry import VisualOdometry, AbsoluteScaleComputer

from cv_bridge import CvBridge

import tf2_ros
#import tf2_geometry_msgs
from tf2_ros import TransformBroadcaster
import transforms3d
import PyKDL


class VisualOdomNode(Node):

    def __init__(self):
        super().__init__("VisualOdomNode")
        self.get_logger().info("VisualOdom Node Started")

        # Params

        # Configuration
        self.declare_parameter('/configuration/imu', True)
        self.declare_parameter('/configuration/msckf', False)
        self.declare_parameter('/configuration/log', False)
        self.declare_parameter('/configuration/active_plot', False)
        self.declare_parameter('/configuration/time_profile', False)
        self.declare_parameter('/configuration/sweep', False)
        # Detector
        self.declare_parameter('/detector/detector_name', 'HandcraftDetector')
        self.declare_parameter('/detector/detector_type', 'SIFT')
        self.declare_parameter('/detector/SIFT/nfeatures', 1300)
        self.declare_parameter('/detector/SIFT/nOctaveLayers', 4)
        self.declare_parameter('/detector/SIFT/contrastThreshold', 0.04)
        self.declare_parameter('/detector/SIFT/edgeThreshold', 10)
        self.declare_parameter('/detector/SIFT/sigma', 1.6)
        # Matcher
        self.declare_parameter('/matcher/matcher_name', 'FrameByFrameMatcher')
        self.declare_parameter('/matcher/matcher_type', 'FLANN')
        self.declare_parameter('/matcher/FLANN/kdTrees', 5)
        self.declare_parameter('/matcher/FLANN/searchChecks', 50)
        self.declare_parameter('/matcher/distance_ratio', 0.75)
        
        self.sequence_idx = 0
        self.bridge = CvBridge()

        #tf_robot = tf.TransformBroadcaster()
        self.br = TransformBroadcaster(self)

        # Servicios desde DataLoader o Sensores
        #Crear cliente para imagen RGB####################################
        self.srv_image = self.create_client(ImageLoader,"/ffulgor/dataloader/image_raw")
        self.srv_pose  = self.create_client( PoseLoader, "/ffulgor/dataloader/pose")
        self.srv_camera_info = self.create_client(CameraInfoLoader, "/ffulgor/dataloader/camera_info")

        while not (self.srv_pose.wait_for_service(timeout_sec=1.0) and  \
                   self.srv_image.wait_for_service(timeout_sec=1.0) and \
                   self.srv_camera_info.wait_for_service(timeout_sec=1.0)):
            self.get_logger().info('Waiting for Dataloader Services')
            
        self.get_logger().info('Dataloader Services OK')

        # Publishers
        self.pose_est_pub = self.create_publisher(PoseStamped, '/ffulgor/visualodom/pose', 10)
        self.image_kp_pub = self.create_publisher(KeyPoints, '/ffulgor/visualodom/kps_image', 10)

        # Adquirir Camera Info
        request = CameraInfoLoader.Request()
        #cam_info = self.srv_camera_info.call(request).camera_info
        future = self.srv_camera_info.call_async(request)

        rclpy.spin_until_future_complete(self, future)
        cam_info = future.result().camera_info
        self.R_tmp_car = np.identity(3)
        
        # Shape parameters to meets original VisualOdom
        param_list = [ [parameter.name.split('/')[-1], parameter.value] for parameter in self._parameters.values()]
        param_list = dict(param_list)
        print(param_list)

        # Instancia Clase Odometria Visual
        self.vo_vins = VisualOdometry(cam_info.k,param_list,self.get_parameter("/configuration/time_profile").value )
        self.absscale = AbsoluteScaleComputer()

        # Transformaciones 
        self.tf_buffer = tf2_ros.Buffer()  # tf buffer length
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer, self)

    def get_vo_kps(self):
        kps = self.vo_vins.kptdescs["cur"]

        keypoints = []
        for i in range(len(kps["keypoints"])):
            kp = KeyPoint()
            kp.pt.x = kps["keypoints"][i][0]
            kp.pt.y = kps["keypoints"][i][1]
            kp.response = kps["scores"][i]
            keypoints.append(kp)
        return keypoints

    def send_transform_robot(self, R, t):
        transform = TransformStamped()

        # Header
        transform.header.stamp = self.get_clock().now().to_msg()
        transform.header.frame_id = 'world'
        transform.child_frame_id = 'base_link'

        # Translation
        transform.transform.translation.x = t[0]
        transform.transform.translation.y = 0. #t[1]
        transform.transform.translation.z = t[2]

        # Rotation
        q = transforms3d.quaternions.mat2quat(R)
        transform.transform.rotation.x = q[1]
        transform.transform.rotation.y = q[2]
        transform.transform.rotation.z = q[3]
        transform.transform.rotation.w = q[0]

        # Send the transformation
        self.br.sendTransform(transform)

    def send_transform_robot_pose(self, pose):
        transform = TransformStamped()

        # Header
        transform.header.stamp = pose.header.stamp
        transform.header.frame_id = pose.header.frame_id
        transform.child_frame_id = 'base_link'

        # Translation
        transform.transform.translation.x = pose.pose.position.x
        transform.transform.translation.y = pose.pose.position.y
        transform.transform.translation.z = pose.pose.position.z

        # Rotation
        transform.transform.rotation.x = pose.pose.orientation.x
        transform.transform.rotation.y = pose.pose.orientation.y
        transform.transform.rotation.z = pose.pose.orientation.z
        transform.transform.rotation.w = pose.pose.orientation.w

        # Send the transformation
        self.br.sendTransform(transform)

    def pose2trans (self, pose, child_id):
        transform = TransformStamped()

        # Header
        transform.header.stamp = pose.header.stamp
        transform.header.frame_id = pose.header.frame_id
        transform.child_frame_id = child_id

        # Translation
        transform.transform.translation.x = pose.pose.position.x
        transform.transform.translation.y = pose.pose.position.y
        transform.transform.translation.z = pose.pose.position.z

        # Rotation
        transform.transform.rotation.x = pose.pose.orientation.x
        transform.transform.rotation.y = pose.pose.orientation.y
        transform.transform.rotation.z = pose.pose.orientation.z
        transform.transform.rotation.w = pose.pose.orientation.w

        return transform

    def trans2pose (self, transform):
        pose = PoseStamped()

        pose.header.stamp = transform.header.stamp
        pose.header.frame_id = transform.header.frame_id
        
        pose.pose.position.x = transform.transform.translation.x
        pose.pose.position.y = transform.transform.translation.y
        pose.pose.position.z = transform.transform.translation.z

        pose.pose.orientation.x = pose.pose.orientation.x
        pose.pose.orientation.y = pose.pose.orientation.y
        pose.pose.orientation.z = pose.pose.orientation.z
        pose.pose.orientation.w = pose.pose.orientation.w

        return pose
        

    ### Remover cuando ROS2 se digne a poner tf2 geometry msgs
    def transform_to_kdl(self,t):
        return PyKDL.Frame(PyKDL.Rotation.Quaternion(t.transform.rotation.x, t.transform.rotation.y,
                                                     t.transform.rotation.z, t.transform.rotation.w),
                           PyKDL.Vector(t.transform.translation.x, 
                                        t.transform.translation.y, 
                                        t.transform.translation.z))
     
    # PoseStamped
    def do_transform_pose(self, pose, transform):
        f = self.transform_to_kdl(transform) * PyKDL.Frame(PyKDL.Rotation.Quaternion(pose.pose.orientation.x, pose.pose.orientation.y,
                                                                              pose.pose.orientation.z, pose.pose.orientation.w),
                                                    PyKDL.Vector(pose.pose.position.x, pose.pose.position.y, pose.pose.position.z))
        res = PoseStamped()
        res.pose.position.x = f.p[0]
        res.pose.position.y = f.p[1]
        res.pose.position.z = f.p[2]
        (res.pose.orientation.x, res.pose.orientation.y, res.pose.orientation.z, res.pose.orientation.w) = f.M.GetQuaternion()
        res.header = transform.header
        return res

    def get_transform( self, tf_buffer, source_frame, target_frame, time):
            try:
                timeout     = Duration()
                timeout.sec = 3.0
                trans = tf_buffer.lookup_transform( target_frame,
                                                    source_frame,
                                                    time,
                                                    timeout)
                #print(trans)
            except Exception as inst:
                print(type(inst))    # the exception instance
                print(inst.args)     # arguments stored in .args
                print(inst)          # __str__ allows args to be printed directly,print(
    
            return trans
    
def main (args=None):

    rclpy.init(args=args)
    node = VisualOdomNode()

    thread = threading.Thread(target=rclpy.spin, args=(node,), daemon=True)
    rate = node.create_rate(100)

    try:
        while rclpy.ok():
            # Pose request (only for scale)
            request = PoseLoader.Request()
            request.sequence = node.sequence_idx
            future = node.srv_pose.call_async(request)
            rclpy.spin_until_future_complete(node, future)
            pose_gt = future.result().pose

            # Image Request
            request = ImageLoader.Request()
            request.sequence = node.sequence_idx
            request.camera = 0 # Gray Image
            future = node.srv_image.call_async(request)
            rclpy.spin_until_future_complete(node, future)
            image_msg = future.result()
            image_cv = node.bridge.imgmsg_to_cv2(image_msg.image)
            # VO Update
            q = [pose_gt.pose.orientation.x,pose_gt.pose.orientation.y,pose_gt.pose.orientation.z,pose_gt.pose.orientation.w]
            R_gt = np.array(transforms3d.quaternions.quat2mat(q))
            t_gt = np.array([pose_gt.pose.position.x,pose_gt.pose.position.y,pose_gt.pose.position.z])
            gt_pose_mat = np.array([[0.,0.,0.,pose_gt.pose.position.x],
                                    [0.,0.,0.,pose_gt.pose.position.y],
                                    [0.,0.,0.,pose_gt.pose.position.z]])

            if (np.count_nonzero(gt_pose_mat) == 0):
                scale = 4.0
            else:
                scale = node.absscale.update(gt_pose_mat)

            ### Update
            if(node.sequence_idx == 0):
                R_gt = np.identity(3)
                t_gt = np.zeros(3)
                node.vo_vins.set_initial_mat(R_gt,t_gt)
                R = R_gt
                t = t_gt
                node.vo_vins.update(image_cv,scale)
            else:
                R,t = node.vo_vins.update(image_cv,scale)
                if ( len(t.shape) > 1 ): # Si es matriz de traslacion
                    t = t[:,0]

            ## Transformar los puntos obtenidos en frame de cámara
            ## al frame de base del robot
            pose_est = PoseStamped()
            
            pose_est.pose.position.x = t[0]
            pose_est.pose.position.y = 0.#t[1]
            pose_est.pose.position.z = t[2]
            q = transforms3d.quaternions.mat2quat(R)
            pose_est.pose.orientation.x = q[1]
            pose_est.pose.orientation.y = q[2]
            pose_est.pose.orientation.z = q[3]
            pose_est.pose.orientation.w = q[0]
            pose_est.header.frame_id = 'world'
            pose_est.header.stamp = image_msg.image.header.stamp

            #### Transformacion de Sistema de la Camara al Sistema del mundo
            ## TODO: Orcos kdl todavia no hizo su release a ros2 para usar
            ## tf2 geometry msgs, cuando se haga se puede reemplazar por
            # algo mas general (tf_buffer.transform())

            ### Transformacion de PUNTO a sistema de referencia del mundo.
            time = node.get_clock().now()
            trans = node.get_transform(node.tf_buffer,'front_cam0_link_optical','base_link',time)
            pose_transformed = node.do_transform_pose(pose_est,trans)
            pose_transformed.pose.orientation.x = q[1]
            pose_transformed.pose.orientation.y = q[3]
            pose_transformed.pose.orientation.z = -q[2]
            pose_transformed.pose.orientation.w = q[0]
            pose_transformed.header.frame_id = 'world'
            pose_transformed.header.stamp = image_msg.image.header.stamp         

            node.pose_est_pub.publish(pose_transformed)
            node.send_transform_robot_pose(pose_transformed)

            # Post Keypoints # Timestamp # Mensaje de tipo imagen##################
            kps_msg = KeyPoints()
            kps_msg.keypoints = node.get_vo_kps()
            kps_msg.header.frame_id = 'world'
            kps_msg.header.stamp = image_msg.image.header.stamp
            node.image_kp_pub.publish(kps_msg)
            node.sequence_idx += 1
            rate.sleep()
    except KeyboardInterrupt:
        pass
    rclpy.shutdown()
    thread.join()
    
if __name__ == '__main__':
    main()
