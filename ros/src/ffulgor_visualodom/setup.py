from setuptools import setup
import warnings
warnings.filterwarnings("ignore")

package_name = 'ffulgor_visualodom'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='lborgnino',
    maintainer_email='leoborgnino@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'visualodom = ffulgor_visualodom.ffulgor_visualodom_node:main'
        ],
    },
)
