#! /usr/bin/env python3

import math
import rclpy
from rclpy.node import Node
import matplotlib.pyplot as plt
import cv2
import numpy as np
import sys
import threading
import matplotlib.cm as cm

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TransformStamped
from sensor_msgs.msg import Image
from sensor_msgs.msg import CameraInfo
from sensor_msgs.msg import PointCloud2
from nav_msgs.msg import Path
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Float64MultiArray

from ffulgor_msgs.msg import DetectionAndRaw
from ffulgor_msgs.msg import KeyPoints, KeyPoint
from ffulgor_msgs.srv import ImageLoader
from ffulgor_msgs.srv import PoseLoader
from ffulgor_msgs.srv import LidarLoader

from cv_bridge import CvBridge

import queue # sync data

class VisualizationNode(Node):

    def __init__(self):
        super().__init__("VisualizationNode")
        self.get_logger().info("Visualization Node Started")

        # Params

        # Configuration
        self.declare_parameter('lidar', False)
        self.declare_parameter('lidar_detection', False)
        self.declare_parameter('visualodom', False)
        self.declare_parameter('ts_recog', False)
        self.declare_parameter('max_depth_fifo', 1500)

        # Parameters to flags
        self.show_lidar      = self.get_parameter('lidar').value
        self.show_lidar_det  = self.get_parameter('lidar_detection').value
        self.show_visualodom = self.get_parameter('visualodom').value
        self.show_ts_recog   = self.get_parameter('ts_recog').value

        self.max_depth_fifo = self.get_parameter('max_depth_fifo').value # Be careful with this, if it is short, could break the pipeline, but this is VISUALIZATION
        #print(self._parameters.values())
        #param_list = [ [parameter.name, parameter.value] for parameter in self._parameters.values()]
        #param_list = dict(param_list)
        #print(param_list)

        self.sequence_idx = 0
        self.bridge = CvBridge()

        # Servicios
        self.srv_image = self.create_client(ImageLoader,"/ffulgor/dataloader/image_raw")
        self.srv_pose_gt = self.create_client(PoseLoader, '/ffulgor/dataloader/pose')
        #self.srv_lidar_raw = self.create_client(LidarLoader, '/ffulgor/dataloader/lidar_raw')

        # Subscriptores
        self.pose_sub = self.create_subscription(PoseStamped, '/ffulgor/visualodom/pose', self.PoseCallback, 10)

        self.kps = []
        self.client_futures = []
        self.kps_sub = self.create_subscription(KeyPoints, '/ffulgor/visualodom/kps_image', self.KpsCallback, 10)
        
        self.lidar_detection_sub = self.create_subscription(Float64MultiArray, '/ffulgor/od_lidar/lidar_detection', self.LidarDetectionCallback, 10)
        self.lidar_detection_raw_sub = self.create_subscription(DetectionAndRaw, '/ffulgor/od_lidar/lidar_detection_and_raw',self.LidarDetectionAndRawCallback,10)

        self.ts_recog_sub = self.create_subscription(Image, '/ffulgor/ts_recog/image_ts_recog',self.TSDetectionCallback,10)

        # Publishers
        self.path_est = Path()
        self.path_est_pub = self.create_publisher(Path, '/ffulgor/visualization/path_visualodom',10)

        self.path_gt = Path()
        self.path_gt_pub = self.create_publisher(Path, '/ffulgor/visualization/path_gt_visualodom',10)

        self.image_kp_vio_pub = self.create_publisher(Image, '/ffulgor/visualization/image_kp_visualodom',10)
        
        self.lidar_detection_vis_pub = self.create_publisher(MarkerArray, '/ffulgor/visualization/lidar_detection_vis',10)
        self.lidar_raw_pub = self.create_publisher(PointCloud2, '/ffulgor/visualization/lidar_raw_visualization',10)

        self.ts_recog_sync_pub = self.create_publisher(Image, '/ffulgor/visualization/image_ts_recog_sync',10)

        # Colas
        self.fifo_visualodom = queue.Queue()
        self.fifo_lidar_detect = queue.Queue()
        self.fifo_ts_recog = queue.Queue()
        self.fifo_pose = queue.Queue()

    def PoseCallback (self, msg):
        self.fifo_pose.put(msg)

    def KpsCallback (self, msg):
        # Image Request
        request = ImageLoader.Request()
        request.sequence = self.sequence_idx
        self.client_futures.append(self.srv_image.call_async(request))
        self.kps.append({  'timestamp': msg.header.stamp,
                           'keypoints': msg.keypoints     })

    def TSDetectionCallback (self, msg):
        if (self.fifo_ts_recog.qsize() <= self.max_depth_fifo):
            self.fifo_ts_recog.put(msg)
        
    def LidarDetectionCallback(self, msg):
        detections = []
        n_detections = msg.layout.dim[0].size
        stride = msg.layout.dim[1].stride
        for i in range(n_detections):
            detections.append(msg.data[i*stride:(i+1)*stride])
        markerarray = self.create_marker_array(detections)
        #self.fifo_lidar_detect.put(markerarray)
        self.lidar_detection_vis_pub.publish(markerarray)

    def LidarDetectionAndRawCallback(self, msg):
        detections = []
        if (msg.detections.layout.dim != []):
            n_detections = msg.detections.layout.dim[0].size
            stride = msg.detections.layout.dim[1].stride
            for i in range(n_detections):
                detections.append(msg.detections.data[i*stride:(i+1)*stride])
            markerarray = self.create_marker_array(detections)
        else:
            markerarray = []
        if (self.fifo_lidar_detect.qsize() <= self.max_depth_fifo):
            self.fifo_lidar_detect.put({'raw': msg.point_cloud, 'detections': markerarray})
        #self.lidar_detection_vis_pub.publish(markerarray)

    def PublishGtPath (self, msg):
        self.path_gt.header = msg.header
        self.path_gt.poses.append(msg)
        self.path_gt_pub.publish(self.path_gt)
        
    # --- VISUALODOM VISUALIZATION ---
    # based on: https://github.com/magicleap/SuperGluePretrainedNetwork/blob/master/models/utils.py
    def plot_keypoints(self,image, kpts, scores=None):
        
        kpts = np.round(kpts).astype(int)

        if scores is not None:
            # get color
            smin, smax = scores.min(), scores.max()
            assert (0 <= smin <= 1 and 0 <= smax <= 1)

            color = cm.gist_rainbow(scores*5.)
            #print(color)
            color = (np.array(color[:, :3]) * 255).astype(int)[:, ::-1]
            #print(color)
            # text = f"min score: {smin}, max score: {smax}"

            for (x, y), c in zip(kpts, color):
                c = (int(c[0]), int(c[1]), int(c[2]))
                cv2.drawMarker(image, (x, y), tuple(c), cv2.MARKER_SQUARE, 10)

        else:
            for x, y in kpts:
                cv2.drawMarker(image, (x, y), (0, 255, 0), cv2.MARKER_CROSS, 10)

        return image
    
    def keypoints_plot(self,img,kps):
        try:
            if img.shape[2] == 1:
                img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
        except:
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
            pass

        kps_points = np.array([np.array([kp.pt.x,kp.pt.y]) for kp in kps])
        kps_scores = np.array([kp.response for kp in kps])
            
        return self.plot_keypoints(img, kps_points, kps_scores)

    
    ## LIDAR VISUALIZATION ##
    
    def create_marker(self, detection, marker_id):
        marker = Marker()
        marker.header.frame_id = 'center_laser_link'
        marker.header.stamp = self.get_clock().now().to_msg()
        
        if marker_id == 100: # To delete all the objects
            marker.action = 3
            return marker
        
        marker.id = marker_id
        marker.ns = 'detections'
        marker.type = 1
        marker.action = 0
        
        marker.pose.position.x = detection[0]
        marker.pose.position.y = detection[1]
        marker.pose.position.z = -0.5 # Fixed z coord for now
        
        marker.scale.x = detection[2]
        marker.scale.y = detection[3]
        marker.scale.z = 2.0 # Fixed height for now
        
        marker.pose.orientation.x = detection[4]
        marker.pose.orientation.y = detection[5]
        marker.pose.orientation.z = detection[6]
        marker.pose.orientation.w = detection[7]
                
        colors = self.choose_color(int(detection[10]))
        marker.color.a = 0.6 # alpha
        marker.color.r = colors[0]
        marker.color.g = colors[1]
        marker.color.b = colors[2]
        
        marker.lifetime.sec = 0
        marker.lifetime.nanosec = 0
        
        marker.frame_locked = False
        return marker
    
    def create_marker_array(self, detections):
        mrk_array = MarkerArray()
        mrk_array.markers.append(self.create_marker(None, 100))
        for idx, detection in enumerate(detections):
            mrk_array.markers.append(self.create_marker(detection, idx))
        return mrk_array
    
    # Green for cars, blue for pedestrians, red for cyclist (r, g, b)
    def choose_color(self, class_predicted):
        colors = [[0.0, 1.0, 0.0], [0.0, 0.0, 1.0], [1.0, 0.0, 0.0]]
        return colors[class_predicted]

def main (args=None):

    rclpy.init(args=args)
    node = VisualizationNode()

    while rclpy.ok():
        rclpy.spin_once(node)
        incomplete_futures = []
        for f in node.client_futures:
            if f.done():
                image_msg = f.result()
                image_cv = node.bridge.imgmsg_to_cv2(image_msg.image)
                image = node.keypoints_plot(image_cv, node.kps.pop(0)['keypoints'])
                image_publish = node.bridge.cv2_to_imgmsg(image)
                image_publish.header.frame_id = 'world'
                if (node.fifo_visualodom.qsize() <= node.max_depth_fifo):
                    node.fifo_visualodom.put(image_publish)
                
                #if (node.srv_lidar_raw.service_is_ready()):
                #    request = LidarLoader.Request()
                #    request.sequence = node.sequence_idx
                #    future = node.srv_lidar_raw.call_async(request)
                #    rclpy.spin_until_future_complete(node, future)
                #    cloud = future.result().lidar_data
                #    node.PublishLidarRaw(cloud)
                #else:
                #    request = PoseLoader.Request()
                #    request.sequence = node.sequence_idx
                #    future = node.srv_pose_gt.call_async(request)
                #    rclpy.spin_until_future_complete(node, future)
                #    pose_gt = future.result().pose
                #    node.PublishGtPath(pose_gt)

                print(node.sequence_idx)
                node.sequence_idx +=1
            else:
                incomplete_futures.append(f)

                
            ### Agregar Lidar Solo, Camara Sola, sin los algoritmos
            flag_lidar_detect_empty = node.fifo_lidar_detect.empty() and node.show_lidar_det
            flag_visualodom_empty = node.fifo_visualodom.empty() and node.show_visualodom
            flag_pose_empty = node.fifo_pose.empty() and node.show_visualodom
            flag_ts_recog_empty = node.fifo_ts_recog.empty() and node.show_ts_recog
                
            if not ( flag_lidar_detect_empty or flag_visualodom_empty or flag_pose_empty or flag_ts_recog_empty):
                ## Visualodom
                if (node.show_visualodom):
                    node.image_kp_vio_pub.publish(node.fifo_visualodom.get())
                    pose_msg = node.fifo_pose.get()
                    node.path_est.header = pose_msg.header
                    node.path_est.poses.append(pose_msg)
                    node.path_est_pub.publish(node.path_est)
                ## Lidar Detect
                if (node.show_lidar_det):
                    lidar_dict = node.fifo_lidar_detect.get()
                    if (lidar_dict['detections'] != []):
                        node.lidar_detection_vis_pub.publish(lidar_dict['detections'])
                    node.lidar_raw_pub.publish(lidar_dict['raw'])
                ## Ts Recog
                #print(node.show_ts_recog)
                if (node.show_ts_recog):
                    node.ts_recog_sync_pub.publish(node.fifo_ts_recog.get())

            node.client_futures = incomplete_futures

    rclpy.shutdown()
    #thread.join()
    
if __name__ == '__main__':
    main()
