#! /usr/bin/env python3

import math
import rclpy
from rclpy.node import Node
import matplotlib.pyplot as plt
import cv2
import numpy as np
import sys
import threading
import matplotlib.cm as cm
import os

from sensor_msgs.msg import Image
from sensor_msgs.msg import CameraInfo
from ffulgor_msgs.srv import ImageLoader
from ffulgor_msgs.srv import CameraInfoLoader

from cv_bridge import CvBridge
from .TSclass import TSrecog

sys.path.append('../')

import warnings # FIXME
warnings.filterwarnings("ignore")

class Traffic_Sign_node(Node):

    def __init__(self):
        super().__init__("Traffic_Sign_node")
        self.get_logger().info("Traffic_Sign Node Started")

        #RGB camera client
        self.srv_image = self.create_client(ImageLoader,"/ffulgor/dataloader/image_raw")
        self.srv_camera_info = self.create_client(CameraInfoLoader, "/ffulgor/dataloader/camera_info")
        
        while not (self.srv_image.wait_for_service(timeout_sec=1.0) and \
                   self.srv_camera_info.wait_for_service(timeout_sec=1.0)):
            self.get_logger().info('Waiting for Dataloader Services')
        
        self.get_logger().info('Dataloader Services OK')        
        
        #self.image_req = Image.Request()
        
        # Adquirir Camera Info
        request = CameraInfoLoader.Request()
        #cam_info = self.srv_camera_info.call(request).camera_info
        future = self.srv_camera_info.call_async(request)

        rclpy.spin_until_future_complete(self, future)
        cam_info = future.result().camera_info
            
        self.sequence_idx = 0
        self.bridge = CvBridge()

        # Publishers
        #self.pose_est_pub = self.create_publisher(PoseStamped, '/ffulgor/visualodom/pose', 10)
        #self.image_kp_pub = self.create_publisher(KeyPoints, '/ffulgor/visualodom/kps_image', 10)
        self.ts_recog_pub = self.create_publisher(Image, '/ffulgor/ts_recog/image_ts_recog',10)

        home = os.environ['ROS_ROOT_PATH']
        
        # Instancia Clase Reconocimiento de Señales
        yolov5_weight_path =  home + "/src/ffulgor_ts_recog/ffulgor_ts_recog/weights/yolov5.pt"
        prohibitory_weight_path = home + "/src/ffulgor_ts_recog/ffulgor_ts_recog/weights/CNN_prohibitory.h5"
        danger_weight_path = home + "/src/ffulgor_ts_recog/ffulgor_ts_recog/weights/CNN_danger.h5"
        other_weight_path = home + "/src/ffulgor_ts_recog/ffulgor_ts_recog/weights/CNN_other.h5"
        mandatory_weight_path = home + "/src/ffulgor_ts_recog/ffulgor_ts_recog/weights/CNN_mandatory.h5"
        classes_csv = home + "/src/ffulgor_ts_recog/ffulgor_ts_recog/43class_names.csv"
        
        self.tsclass = TSrecog(yolov5_weight_path, prohibitory_weight_path, danger_weight_path, other_weight_path, mandatory_weight_path, classes_csv)

def main (args=None):

    rclpy.init(args=args)
    node = Traffic_Sign_node()

    thread = threading.Thread(target=rclpy.spin, args=(node,), daemon=True)
    rate = node.create_rate(100)
    
    yolov5_model, classification_model_danger, classification_model_mandatory, classification_model_other, classification_model_prohibitory, classes = node.tsclass.load_models()
    
    try:
        while rclpy.ok():
            # Image Request
            request = ImageLoader.Request()
            request.sequence = node.sequence_idx
            request.camera = 3 #RGB Camera
            future = node.srv_image.call_async(request)
            rclpy.spin_until_future_complete(node, future)
            
            try:
                image_msg = future.result()
                image_cv = node.bridge.imgmsg_to_cv2(image_msg.image)

            except Exception as e:
                node.get_logger().info('Service failed %r' % (e,))
                
            else:
                node.get_logger().info('Processing image %d' % (node.sequence_idx))
         
                image_out, bboxes, signs, predictions = node.tsclass.detect_classify(image_cv, yolov5_model, classification_model_danger, classification_model_mandatory, classification_model_other, classification_model_prohibitory, classes)
                
                image_out = cv2.cvtColor(image_out, cv2.COLOR_RGB2BGR)
                #Save all frames on local directory
                #file_path = '/home/nico/Escritorio/images_1/señal_detectada_' + str(node.sequence_idx) + ".jpg"
                #cv2.imwrite(file_path, image_out)

                image_publish = node.bridge.cv2_to_imgmsg(image_out)
                image_publish.header.frame_id = 'world'
                node.ts_recog_pub.publish(image_publish)

                if not signs:
                    node.get_logger().info('No Signs')
                else:
                    for i, sign in enumerate(signs):
                        node.get_logger().info(f'Sign: {sign} - bbox(xmin,xmax,ymin,ymax): {bboxes[i]}')

                    #Save detected frames on local directory
                    #file_path = '/home/nico/Escritorio/images_1/señal_detectada_' + str(node.sequence_idx) + ".jpg"
                    #image_out = cv2.cvtColor(image_out, cv2.COLOR_RGB2BGR)
                    #cv2.imwrite(file_path, image_out)
                    
                node.sequence_idx += 1
                rate.sleep()

    except KeyboardInterrupt:
        pass
    rclpy.shutdown()
    thread.join()

if __name__ == '__main__':
    main()
