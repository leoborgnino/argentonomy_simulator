# -*- coding: utf-8 -*-

import time
import cv2
import torch
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from keras.models import load_model

from functions import *


################################# Prueba #######################################

weight_path = '~/ffulgor_simulator/ros/src/ffulgor_ts_recog/ffulgor_ts_recog/weights/yolov5.pt'

all_classes = pd.read_csv('~/ffulgor_simulator/ros/src/ffulgor_ts_recog/ffulgor_ts_recog/scripts/43class_names.csv')
classes = list(all_classes['names'])

#Modelos de detección(yolo) y clasificación(CNN)
classification_model_danger = keras.models.load_model('~/ffulgor_simulator/ros/src/ffulgor_ts_recog/ffulgor_ts_recog/weights/CNN2_AD_danger.h5')
classification_model_mandatory = keras.models.load_model('~/ffulgor_simulator/ros/src/ffulgor_ts_recog/ffulgor_ts_recog/weights/CNN2_AD_mandatory.h5')
classification_model_other = keras.models.load_model('~/ffulgor_simulator/ros/src/ffulgor_ts_recog/ffulgor_ts_recog/weights/CNN2_AD_other.h5')
classification_model_prohibitory = keras.models.load_model('~/ffulgor_simulator/ros/src/ffulgor_ts_recog/ffulgor_ts_recog/weights/CNN2_AD_prohibitory.h5')

detection_model = yolov5_model(weight_path)

############### IMAGEN ###################
image = cv2.imread("/content/drive/MyDrive/traffic_sign_detection/YOLO_43classes/custom/data/traffic_sign.jpg")
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
imagen_detectada, señales_detectadas, predicciones = detect_classify (image, detection_model, classification_model_prohibitory, classification_model_danger, classification_model_other, classification_model_mandatory, classes)

plt.figure(1,figsize=(20,20))
plt.imshow(imagen_detectada)

############### VIDEO ####################
cap = cv2.VideoCapture('/content/drive/MyDrive/traffic_sign_detection/YOLO_43classes/custom/data/video_to_test.mp4')
ancho = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
alto = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

#output info
fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
out = cv2.VideoWriter('output.avi', fourcc, 25, (ancho,alto))

while (cap.isOpened()):
    check, frame = cap.read()
    if check == True:
        frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        prev_time = time.time()
        detected_frame, detected_signs, predictions = detect_classify(frame_rgb, detection_model, classification_model_prohibitory, classification_model_danger, classification_model_other, classification_model_mandatory, classes)
        detected_frame = cv2.cvtColor(detected_frame, cv2.COLOR_RGB2BGR)
        finish_time = time.time()
        out.write(detected_frame)
    else:
        break
        
    if (cv2.waitKey(1) & 0xff == ord('q')):
            break

cap.release()
out.release()
cv2.destroyAllWindows()

