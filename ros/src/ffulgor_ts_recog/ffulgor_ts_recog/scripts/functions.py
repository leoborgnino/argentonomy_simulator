# -*- coding: utf-8 -*-

import time
import cv2
import torch
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from keras.models import load_model

#Crear modelo de yolov5
def yolov5_model(weight_path):
  model = torch.hub.load('ultralytics/yolov5', 'custom', weight_path)
  model = model.autoshape()  # for PIL/cv2/np inputs and NMS
  model.conf = 0.85 #model confidence
  return model

#Encontrar señales de tránsito
def detect_sign(image, model):
  # Inference
  detections = model(image, size=640)
  detections = detections.pandas().xyxy[0]
  return detections

#Recortar los recuadros donde hay una señal
def cut_boxes(detections, image):
  #results debe estar en formato pandas
  #image debe estar en el rango 0 a 255 (uint8)
  xmin, xmax, ymin, ymax, cutted_images, bboxes, confidence, sign = [], [], [], [], [], [], [], []
  width, height = 64,64
  image = image/255
  for i in range(len(detections)):
    xmin.append(int(detections['xmin'][i]))
    xmax.append(int(detections['xmax'][i]))
    ymin.append(int(detections['ymin'][i]))
    ymax.append(int(detections['ymax'][i]))
    bboxes.append((xmin[i],xmax[i],ymin[i],ymax[i]))
    confidence.append(round(detections['confidence'][i],2))
    sign.append(detections['name'][i])
    try:
        cutted_images.append(cv2.resize(image[ymin[i]:ymax[i], xmin[i]:xmax[i]],(width,height)))
    except cv2.error as e:
        print('Invalid frame!')
  
  return cutted_images, bboxes, confidence, sign

#Modelo de CNN de 43 clases distintas
def classify_sign(image, model, classes):
  image = np.expand_dims(image, axis=0) #shape: (1,32,32,3)
  prediction = model.predict(image)
  sign_class = classes[np.argmax(prediction)]
  pred = round(np.amax(prediction),2)
  return sign_class, pred 

#4 Modelos de CNN, uno para cada tipo de señal
def classify_sign_4CNN(image, model_prohibitory, model_danger, model_other, model_mandatory, sign, classes):

  #Diccionarios para interpretar las salidas de las CNN: Las salidas van de 0 a la longitud de cada diccionario, y cada salida se corresponde con un label del dataset
  mandatory_class_dict = {0:33,1:34,2:35,3:36,4:37,5:38,6:39,7:40}
  prohibitory_class_dict = {0:0,1:1,2:2,3:3,4:4,5:5,6:7,7:8,8:9,9:10,10:15,11:16}
  danger_class_dict = {0:11,1:18,2:19,3:20,4:21,5:22,6:23,7:24,8:25,9:26,10:27,11:28,12:29,13:30,14:31}
  other_class_dict = {0:6,1:12,2:13,3:14,4:17,5:32,6:41,7:42}

  image = np.expand_dims(image, axis=0) #shape: (1,32,32,3)
  if sign == 'prohibitory':
    prediction = model_prohibitory.predict(image)
    prediction_sign = prohibitory_class_dict[np.argmax(prediction)]
  if sign == 'mandatory':
    prediction = model_mandatory.predict(image)
    prediction_sign = mandatory_class_dict[np.argmax(prediction)]
  if sign == 'other':
    prediction = model_other.predict(image)
    prediction_sign = other_class_dict[np.argmax(prediction)]
  if sign == 'danger':
    prediction = model_danger.predict(image)
    prediction_sign = danger_class_dict[np.argmax(prediction)]
    
  sign_class = classes[prediction_sign]
  pred = round(np.amax(prediction),2)
  return sign_class, pred 

#Dibujar los recuadros en imagen
def draw_boxes(bboxes, signs, predictions, original_image):
  color = (255,0,0)
  image_cp = np.copy(original_image)

  #Dimensiones originales de la imagen
  width_original = image_cp.shape[1]
  height_original = image_cp.shape[0]
  
  for i, bbox in enumerate(bboxes):
    ###bbox = (xmin,xmax,ymin,ymax)
    try:
      cv2.rectangle(image_cp, (bbox[0], bbox[2]), (bbox[1], bbox[3]), color, 1)
      cv2.putText(image_cp,f"{signs[i]} [{round(predictions[i],4)}]", (bbox[0], bbox[2] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 1)
    except cv2.error as e:
      print('Invalid frame!')
  return image_cp

#Integracion de funciones
def detect_classify (image, yolov5_model, classification_model_prohibitory, classification_model_danger, classification_model_other, classification_model_mandatory, classes):
  #Detectamos señales
  detections = detect_sign(image, yolov5_model)

  #Recortamos los bboxes
  cutted_images, bboxes, confidence, sign = cut_boxes(detections, image)

  #Clasificamos cada señal recortada
  signs, predictions = [], []
  for i in range(len(cutted_images)):
    #sign_class, prediction = classify_sign(cutted_images[i], classification_model, classes) #Usando modelo de clasificacion de 43 clases
    sign_class, prediction = classify_sign_4CNN(cutted_images[i], classification_model_prohibitory, classification_model_danger, classification_model_other, classification_model_mandatory, sign[i], classes)
    signs.append(sign_class)
    predictions.append(prediction)

  #Imagen de salida
  image_out = draw_boxes(bboxes,signs, predictions, image)
  return image_out, signs, predictions
