# pip install -r requirements.txt

torch>=1.7.0
torchvision>=0.8.1
opencv-python>=4.1.2
numpy>=1.18.5
torchsummary==1.5.1
matplotlib>=3.2.2
pandas>=1.1.4
tensorflow==2.7.0
seaborn>=0.11.0
Pillow>=7.1.2
requests>=2.23.0
tqdm>=4.41.0
PyYAML>=5.3.1
scipy>=1.4.1
