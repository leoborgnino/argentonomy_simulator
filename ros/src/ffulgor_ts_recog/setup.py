from setuptools import setup
import warnings
warnings.filterwarnings("ignore")

package_name = 'ffulgor_ts_recog'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='nico',
    maintainer_email='nico@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
        	'ts_recog = ffulgor_ts_recog.ffulgor_ts_node:main'
        ],
    },
)
