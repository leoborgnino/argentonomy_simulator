import cv2
import os
import numpy as np
import glob
from tqdm import tqdm
import logging
from .PinholeCamera import PinholeCamera


class SequenceImageLoader(object):
    default_config = {
        "dataset_path": "/home/zxm/Pictures/Webcam",
        "start": 0,
        "format": "jpeg"
    }

    def __init__(self, config={}):
        self.config = self.default_config
        self.config = {**self.config, **config}
        logging.info("Sequence image loader config: ")
        logging.info(self.config)

        self.cam = PinholeCamera(640.0,480.0,731.9459, 723.786, 321.8525, 258.034)

        self.img_id = self.config["start"]
        self.img_N = len(glob.glob(pathname=os.environ['ROS_ROOT_PATH']+'/'+self.config["dataset_path"] + "/*." + self.config["format"]))

        print(os.environ['ROS_ROOT_PATH']+'/'+self.config["dataset_path"] + "/*." + self.config["format"])
        
        self.gt_poses = []
        for i in range(self.img_N):
            pose = np.array([0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
            pose.resize([3,4])
            self.gt_poses.append(pose)
        print(self.gt_poses[0])

    
    def get_cur_pose(self):
        return self.gt_poses[self.img_id - 1]

    def get_pose_by_idx(self, idx):
        idx_p_start = idx + self.config["start"]
        # Check if idx is valid
        if ( idx_p_start >= len(self.gt_poses)):
            idx_p_start = len(self.gt_poses)-1
        return self.gt_poses[idx_p_start]

    def get_timestamp_by_idx(self, idx):
        return 0. #TODO
        #return self.timestamps[idx + self.config["start"]]

    def get_cur_timestamp(self):
        return 0.

    def __getitem__(self, item):
        idx_p_start = item + self.config["start"]
        # Check if idx is valid
        if ( idx_p_start >= len(self.gt_poses)):
            idx_p_start = len(self.gt_poses)-1
        
        file_name = os.environ['ROS_ROOT_PATH']+'/'+self.config["dataset_path"] + "/" + str(idx_p_start) + "-0" + "." + self.config["format"]
        img = cv2.imread(file_name)
        mtx = np.array([[self.cam.fx, 0, self.cam.cx],
                        [0, self.cam.fy, self.cam.cy],
                        [0, 0, 1]])
        dist = np.array([-0.89886433,0.70948496,-0.04938697,0.00765761,-1.10896843])
        
        dst = cv2.undistort(img, mtx, dist)
        
        return dst

    def get_image_by_idx(self, item, camera = 0):
        idx_p_start = item + self.config["start"]
        # Check if idx is valid
        if ( idx_p_start >= len(self.gt_poses)):
            idx_p_start = len(self.gt_poses)-1
        
        file_name = os.environ['ROS_ROOT_PATH']+'/'+self.config["dataset_path"] + "/" + str(idx_p_start) + "-0" + "." + self.config["format"]
        img = cv2.imread(file_name)
        mtx = np.array([[self.cam.fx, 0, self.cam.cx],
                        [0, self.cam.fy, self.cam.cy],
                        [0, 0, 1]])
        dist = np.array([-0.89886433,0.70948496,-0.04938697,0.00765761,-1.10896843])
        
        dst = cv2.undistort(img, mtx, dist)
        
        return dst

    def __iter__(self):
        return self

    def __next__(self):
        if self.img_id < self.img_N:
            img = self.__getitem__(self.img_id)

            self.img_id += 1

            return img
        raise StopIteration()

    def __len__(self):
        return self.img_N - self.config["start"]
