#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import scipy.io as sp
import numpy as np
import os.path

class FordLoader():
    def __init__(self, conf=None):
        pass
    
    def get_velo_by_idx(self, idx):
        idx = f"{idx:04d}"
        file_name = 'Scan' + idx + '.mat'
        path = os.path.expandvars('$ROS_ROOT_PATH')
        path = path.replace('/ros', '/datasets')
        path = path +'/FORD/SCANS/' + file_name
        
        xyz = np.transpose(sp.loadmat(path)['SCAN']['XYZ'][0].tolist()[0])
        reflectivity = sp.loadmat(path)['SCAN']['reflectivity'][0].tolist()[0]
        reflectivity = (reflectivity/256).round(2)
        reflectivity = reflectivity/1.25 # Correction factor

        velo = np.zeros((xyz.__len__(), 4), float)
        velo[:, 0:3] = xyz
        velo[:, 3] = reflectivity[:, 0]
        
        return velo
    
        