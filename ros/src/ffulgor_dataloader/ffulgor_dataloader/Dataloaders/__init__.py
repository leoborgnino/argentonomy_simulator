from .KITTILoader import KITTILoader
from .SequenceImageLoader import SequenceImageLoader
from .FordLoader import FordLoader

def create_dataloader(conf):
    print(conf)
    try:
        code_line = f"{conf['dataset_name']}(conf)"
        loader = eval(code_line)
    except NameError:
        raise NotImplementedError(f"{conf['dataset_name']} is not implemented yet.")

    return loader
