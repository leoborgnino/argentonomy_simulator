import cv2
import os
import numpy as np
import glob
from tqdm import tqdm
import logging
import pykitti

from .LidarData import LidarData
from .PinholeCamera import PinholeCamera


class KITTILoader(object):
    default_config = {
        "dataset_path": os.environ['ROS_ROOT_PATH']+"/../datasets/KITTI",
        "dataset_type": 'raw',
        "sequence": "00",
        "start": 0
    }

    def __init__(self, config={}):
        self.config = self.default_config
        self.config = {**self.config, **config}
        logging.info("KITTI Dataset config: ")
        logging.info(self.config)

        if self.config["sequence"] in ["00", "01", "02"]:
            self.cam = PinholeCamera(1241.0, 376.0, 718.8560, 718.8560, 607.1928, 185.2157)
        elif self.config["sequence"] in ["03"]:
            self.cam = PinholeCamera(1242.0, 375.0, 721.5377, 721.5377, 609.5593, 172.854)
        elif self.config["sequence"] in ["04", "05", "06", "07", "08", "09", "10"]:
            self.cam = PinholeCamera(1226.0, 370.0, 707.0912, 707.0912, 601.8873, 183.1104)
        else:
            self.cam = PinholeCamera(1241.0, 376.0, 718.8560, 718.8560, 607.1928, 185.2157)

        if (self.config["dataset_type"] == 'odometry'):
            self.kitti_read = pykitti.odometry(os.environ['ROS_ROOT_PATH']+'/'+self.config["dataset_path"],self.config["sequence"])
        else:
            date = self.config["dataset_path"].split('/')[-2]
            path = '/'.join(self.config["dataset_path"].split('/')[:-2]) + '/'
            print(self.config["dataset_path"].split('/'))
            self.kitti_read = pykitti.raw(os.environ['ROS_ROOT_PATH']+'/'+path,date,self.config["sequence"])
        # image id
        self.img_id = self.config["start"]

        
    ### Get Transformation Matrix

    def get_T_cam_velo (self, indx ):
        field_idx = self.kitti_read.calib._fields.index('T_cam%d_velo'%(indx))
        matrix_project = self.kitti_read.calib[field_idx]
        return matrix_project[0:3]
        
    #### GET LIDAR
        
    def get_velo_by_idx(self, idx):
        idx_p_start = idx + self.config["start"]
        # Check if idx is valid
        if ( idx_p_start >= len(self.kitti_read.timestamps)):
            idx_p_start = len(self.kitti_read.timestamps)-1
        velo_scan = self.kitti_read.get_velo(idx_p_start)
        velo_scan_filtered = velo_scan #LidarData.filter_by_camera_angle(velo_scan)
        return velo_scan_filtered
        
    ### Get POSE

    def get_cur_pose(self):
        if (self.config["dataset_type"] == 'odometry'):
            return self.kitti_read.poses[self.img_id - 1]
        else:
            return self.kitti_read.oxts[self.img_id - 1].T_w_imu

    def get_pose_by_idx(self, idx):
        idx_p_start = idx + self.config["start"]
        if (self.config["dataset_type"] == 'odometry'):
            # Check if idx is valid
            if ( idx_p_start >= len(self.kitti_read.poses)):
                idx_p_start = len(self.kitti_read.poses)-1
            return self.kitti_read.poses[idx_p_start]
        else:
            # Check if idx is valid
            if ( idx_p_start >= len(self.kitti_read.oxts)):
                idx_p_start = len(self.kitti_read.oxts)-1
            return self.kitti_read.oxts[idx_p_start].T_w_imu

    ###### GET TIMESTAMP
    def get_timestamp_by_idx(self, idx):
        idx_p_start = idx + self.config["start"]
        # Check if idx is valid
        if ( idx_p_start >= len(self.kitti_read.timestamps)):
            idx_p_start = len(self.kitti_read.timestamps)-1
        timestamp = self.kitti_read.timestamps[idx_p_start]
        if (self.config["dataset_type"] == 'odometry'):
            timestamp = timestamp.total_seconds()
        else:
            timestamp = timestamp.timestamp()
        
        return timestamp

    def get_cur_timestamp(self):
        timestamp = self.kitti_read.timestamps[self.img_id - 1]
        if (self.config["dataset_type"] == 'odometry'):
            timestamp = timestamp.total_seconds()
        else:
            timestamp = timestamp.timestamp()
        return timestamp

    def get_image_by_idx(self, item, camera):

        # Index
        idx_p_start = item + self.config["start"]
        if (self.config["dataset_type"] == 'odometry'):
            # Check if idx is valid
            if ( idx_p_start >= len(self.kitti_read.poses)):
                idx_p_start = len(self.kitti_read.poses)-1
        else:
            # Check if idx is valid
            if ( idx_p_start >= len(self.kitti_read.oxts)):
                idx_p_start = len(self.kitti_read.oxts)-1

        # Select Camera
        if (camera == 0):
            img = np.array(self.kitti_read.get_cam0(idx_p_start))
        elif (camera == 1):
            img = np.array(self.kitti_read.get_cam1(idx_p_start))
        elif (camera == 2):
            img = np.array(self.kitti_read.get_cam2(idx_p_start))
        else:
            img = np.array(self.kitti_read.get_cam3(idx_p_start))
            
        return img

    def __getitem__(self, item):
        idx_p_start = item + self.config["start"]
        if (self.config["dataset_type"] == 'odometry'):
            # Check if idx is valid
            if ( idx_p_start >= len(self.kitti_read.poses)):
                idx_p_start = len(self.kitti_read.poses)-1
        else:
            # Check if idx is valid
            if ( idx_p_start >= len(self.kitti_read.oxts)):
                idx_p_start = len(self.kitti_read.oxts)-1
        img = np.array(self.kitti_read.get_cam0(idx_p_start))
        return img

    def __iter__(self):
        return self

    def __next__(self):
        if self.img_id < self.img_N:
            img = self.kitti_read.get_cam0(self.img_id)
            self.img_id += 1
            return img
        raise StopIteration()

    def __len__(self):
        return self.img_N - self.config["start"]
