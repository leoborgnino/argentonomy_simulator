#! /usr/bin/env python3

import os
import math
import rclpy
import sys
import numpy as np
import scipy.io as sp
from rclpy.node import Node
from rclpy.duration import Duration
import sys

import ctypes
import struct    

from std_msgs.msg import Float64
from std_msgs.msg import String
from std_msgs.msg import Header

from ffulgor_msgs.srv import ImageLoader
from ffulgor_msgs.srv import PoseLoader
from ffulgor_msgs.srv import CameraInfoLoader
from ffulgor_msgs.srv import LidarLoader

from geometry_msgs.msg import TransformStamped
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import Image
from sensor_msgs.msg import CameraInfo
from sensor_msgs.msg import PointField
from sensor_msgs.msg import PointCloud2

from cv_bridge import CvBridge

import tf2_ros
from tf2_ros import TransformBroadcaster
import transforms3d
import PyKDL

sys.path.append(os.path.expandvars('$ROS_ROOT_PATH/src/ffulgor_dataloader/ffulgor_dataloader'))

from Dataloaders import create_dataloader

# MOVE ME TO LIDAR DATA
_DATATYPES = {}
_DATATYPES[PointField.INT8]    = ('b', 1)
_DATATYPES[PointField.UINT8]   = ('B', 1)
_DATATYPES[PointField.INT16]   = ('h', 2)
_DATATYPES[PointField.UINT16]  = ('H', 2)
_DATATYPES[PointField.INT32]   = ('i', 4)
_DATATYPES[PointField.UINT32]  = ('I', 4)
_DATATYPES[PointField.FLOAT32] = ('f', 4)
_DATATYPES[PointField.FLOAT64] = ('d', 8)


class DataLoaderNode(Node):

    def __init__(self):
        super().__init__("DataloaderNode")
        self.get_logger().info("Dataloader Node Started")

        # Params
        self.declare_parameter('dataset_name', 'FordLoader') # 'FordLoader'; 'KITTILoader'
        self.declare_parameter('dataset_path', '../datasets/FORD/') # '../datasets/FORD/'; '../datasets/KITTI/'
        self.declare_parameter('dataset_type', 'ford_lidar') # 'ford_lidar'; 'raw_lidar' 
        self.declare_parameter('sequence', '0009')
        self.declare_parameter('start', 0)

        ### Config variable
        param_list = [ [parameter.name.split('/')[-1], parameter.value] for parameter in self._parameters.values()]
        param_list = dict(param_list)
        
        self.bridge = CvBridge()
        self.loader = create_dataloader(param_list)

        ### Service Servers
        srv_image = self.create_service( ImageLoader, "/ffulgor/dataloader/image_raw", self.ReceiveImageRequest)
        srv_pose  = self.create_service(PoseLoader,"/ffulgor/dataloader/pose", self.ReceivePoseRequest)
        srv_camera_info = self.create_service(CameraInfoLoader, "/ffulgor/dataloader/camera_info", self.ReceiveCamInfoRequest)

        print(param_list["dataset_type"])
        if (param_list["dataset_type"] == 'raw_lidar'):
            srv_lidar = self.create_service(LidarLoader, "/ffulgor/dataloader/lidar_raw", self.ReceiveLidarPcRequest)
        if (param_list["dataset_type"] == 'ford_lidar'):
            srv_lidar = self.create_service(LidarLoader, "/ffulgor/dataloader/lidar_raw", self.ReceiveLidarPcRequest_ford)
            
        ## Dataloader Transform
        self.br = TransformBroadcaster(self)
        # Transformaciones 
        self.tf_buffer = tf2_ros.Buffer()  # tf buffer length
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer, self)


    ### Service callbacks
        
    def ReceiveLidarPcRequest(self, req, cloud):

        time = self.loader.get_timestamp_by_idx(req.sequence)
        time_s = int(time)
        time_ns = (time - time_s)*1e09
        header = Header()
        header.stamp = rclpy.time.Time(seconds=time_s,nanoseconds=time_ns).to_msg()
        header.frame_id = 'center_laser_link'

        velo = self.loader.get_velo_by_idx(req.sequence)

        fields = [PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
                  PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
                  PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1),
                  PointField(name='intensity', offset=12, datatype=PointField.FLOAT32, count=1)]

        #cloud_msg = PointCloud2()
        cloud_msg = self.create_cloud(header, fields, velo)
        cloud.lidar_data = cloud_msg
        #print("%d/%d"%(req.sequence,len(self.loader.gt_poses)))
        
        return cloud
    
    def ReceiveLidarPcRequest_ford(self, req, cloud): # .....(self, request, response)

        header = Header()
        header.stamp = self.get_clock().now().to_msg()
        header.frame_id = 'gt_robot'

        velo = self.loader.get_velo_by_idx(req.sequence) 

        fields = [PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
                  PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
                  PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1),
                  PointField(name='intensity', offset=12, datatype=PointField.FLOAT32, count=1)]

        cloud_msg = self.create_cloud(header, fields, velo)
        cloud.lidar_data = cloud_msg
        
        return cloud

    def ReceiveImageRequest(self, req, response):

        response.image = self.bridge.cv2_to_imgmsg(self.loader.get_image_by_idx(req.sequence,req.camera), encoding="passthrough")

        time = self.loader.get_timestamp_by_idx(req.sequence)
        time_s = int(time)
        time_ns = (time - time_s)*1e09
        response.image.header.stamp = rclpy.time.Time(seconds=time_s,nanoseconds=time_ns).to_msg()

        #print("%d/%d"%(req.sequence,len(self.loader.gt_poses)))
        
        return response

    def ReceivePoseRequest(self, req, pose_message):

        time = self.loader.get_timestamp_by_idx(req.sequence)
        time_s = int(time)
        time_ns = (time - time_s)*1e09

        pose_message.pose.header.stamp = rclpy.time.Time(seconds=time_s,nanoseconds=time_ns).to_msg()
        pose_message.pose.header.frame_id = 'world'

        pose = self.loader.get_pose_by_idx(req.sequence)
        pose_message.pose.pose.position.x = pose[0,3]
        pose_message.pose.pose.position.y = pose[1,3]
        pose_message.pose.pose.position.z = pose[2,3]

        R = pose[np.ix_([0,1,2],[0,1,2])]
        q = transforms3d.quaternions.mat2quat(R)
        #print(R)
        #print(q)
        pose_message.pose.pose.orientation.x = q[1]
        pose_message.pose.pose.orientation.y = q[2]
        pose_message.pose.pose.orientation.z = q[3]
        pose_message.pose.pose.orientation.w = q[0]

        
        time = self.get_clock().now()
        trans = self.get_transform(self.tf_buffer,'front_cam0_link_optical','base_link',time)
        #trans = self.get_transform(self.tf_buffer,'base_link','front_cam0_link_optical',time)
        pose_message.pose = self.do_transform_pose(pose_message.pose,trans)
        pose_message.pose.header.frame_id = 'world'

        #pose_message.pose.pose.orientation.x = q[1]
        #pose_message.pose.pose.orientation.y = q[2]
        #pose_message.pose.pose.orientation.z = q[3]
        #pose_message.pose.pose.orientation.w = q[0]
        #print(transforms3d.quaternions.quat2mat([pose_message.pose.pose.orientation.w,pose_message.pose.pose.orientation.x,pose_message.pose.pose.orientation.y,pose_message.pose.pose.orientation.z]))
        #array=[pose_message.pose.pose.position.x,pose_message.pose.pose.position.y,pose_message.pose.pose.position.z]
        
        #print(array)

        #print(trans)
        self.send_tf_from_pose(pose_message)

        return pose_message

    def ReceiveCamInfoRequest(self, req, cam_info_message):

        time = self.loader.get_timestamp_by_idx(req.sequence)
        time_s = int(time)
        time_ns = (time - time_s)*1e09
        cam_info_message.camera_info.header.stamp = rclpy.time.Time(seconds=time_s,nanoseconds=time_ns).to_msg()

        cam_info = self.loader.cam
        cam_info_message.camera_info.height = int(cam_info.height)
        cam_info_message.camera_info.height = int(cam_info.width)
        cam_info_message.camera_info.d = cam_info.d
        cam_info_message.camera_info.k = [cam_info.fx,0.,cam_info.cx,0.,cam_info.fy,cam_info.cy,0.,0.,1.]
        #cam_info_message.camera_info.p = self.loader.get_T_cam_velo(0)

        return cam_info_message # TODO: Agregar rotacion

    ## Extra Methods

    def send_tf_from_pose(self, pose_message):
        transform = TransformStamped()

        # Header
        transform.header.stamp = pose_message.pose.header.stamp
        transform.header.frame_id = pose_message.pose.header.frame_id
        transform.child_frame_id = 'gt_robot'

        # Translation
        transform.transform.translation.x = pose_message.pose.pose.position.x
        transform.transform.translation.y = pose_message.pose.pose.position.y
        transform.transform.translation.z = pose_message.pose.pose.position.z

        # Rotation
        transform.transform.rotation.x = pose_message.pose.pose.orientation.x
        transform.transform.rotation.y = pose_message.pose.pose.orientation.y
        transform.transform.rotation.z = pose_message.pose.pose.orientation.z
        transform.transform.rotation.w = pose_message.pose.pose.orientation.w

        # Send the transformation
        self.br.sendTransform(transform)
        
    ### Remover cuando ROS2 se digne a poner tf2 geometry msgs
    def transform_to_kdl(self,t):
        return PyKDL.Frame(PyKDL.Rotation.Quaternion(t.transform.rotation.x, t.transform.rotation.y,
                                                     t.transform.rotation.z, t.transform.rotation.w),
                           PyKDL.Vector(t.transform.translation.x, 
                                        t.transform.translation.y, 
                                        t.transform.translation.z))
     
    # PoseStamped
    def do_transform_pose(self, pose, transform):
        f = self.transform_to_kdl(transform) * PyKDL.Frame(PyKDL.Rotation.Quaternion(pose.pose.orientation.x, pose.pose.orientation.y,
                                                                              pose.pose.orientation.z, pose.pose.orientation.w),
                                                    PyKDL.Vector(pose.pose.position.x, pose.pose.position.y, pose.pose.position.z))
        res = PoseStamped()
        res.pose.position.x = f.p[0]
        res.pose.position.y = f.p[1]
        res.pose.position.z = f.p[2]
        (res.pose.orientation.x, res.pose.orientation.y, res.pose.orientation.z, res.pose.orientation.w) = f.M.GetQuaternion()
        res.header = transform.header
        return res

    def get_transform( self, tf_buffer, source_frame, target_frame, time):
            try:
                timeout     = Duration()
                timeout.sec = 3.0
                trans = tf_buffer.lookup_transform( target_frame,
                                                    source_frame,
                                                    time,
                                                    timeout)
                #print(trans)
            except Exception as inst:
                print(type(inst))    # the exception instance
                print(inst.args)     # arguments stored in .args
                print(inst)          # __str__ allows args to be printed directly,print(
    
            return trans

    # Transform header and points to Pc2 msg
    def create_cloud(self, header, fields, points):
        """
        Create a L{sensor_msgs.msg.PointCloud2} message.
        @param header: The point cloud header.
        @type  header: L{std_msgs.msg.Header}
        @param fields: The point cloud fields.
        @type  fields: iterable of L{sensor_msgs.msg.PointField}
        @param points: The point cloud points.
        @type  points: list of iterables, i.e. one iterable for each point, with the
                   elements of each iterable being the values of the fields for 
                   that point (in the same order as the fields parameter)
        @return: The point cloud.
        @rtype:  L{sensor_msgs.msg.PointCloud2}
        """

        cloud_struct = struct.Struct(self._get_struct_fmt(False, fields))

        buff = ctypes.create_string_buffer(cloud_struct.size * len(points))

        point_step, pack_into = cloud_struct.size, cloud_struct.pack_into
        offset = 0
        for p in points:
            pack_into(buff, offset, *p)
            offset += point_step

        return PointCloud2(header=header,
                           height=1,
                           width=len(points),
                           is_dense=False,
                           is_bigendian=False,
                           fields=fields,
                           point_step=cloud_struct.size,
                           row_step=cloud_struct.size * len(points),
                           data=buff.raw)
    
    def _get_struct_fmt(self, is_bigendian, fields, field_names=None):
        fmt = '>' if is_bigendian else '<'

        offset = 0
        for field in (f for f in sorted(fields, key=lambda f: f.offset) if field_names is None or f.name in field_names):
            if offset < field.offset:
                fmt += 'x' * (field.offset - offset)
                offset = field.offset
            if field.datatype not in _DATATYPES:
                print('Skipping unknown PointField datatype [%d]' % field.datatype, file=sys.stderr)
            else:
                datatype_fmt, datatype_length = _DATATYPES[field.datatype]
                fmt    += field.count * datatype_fmt
                offset += field.count * datatype_length

        return fmt

def main (args=None):
    rclpy.init(args=args)
    node = DataLoaderNode()
    rclpy.spin(node)
    rclpy.shutdown()

if __name__ == '__main__':
    main()
