from setuptools import setup
import warnings
warnings.filterwarnings("ignore")

package_name = 'ffulgor_dataloader'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools','cv_bridge'],
    zip_safe=True,
    maintainer='lborgnino',
    maintainer_email='leoborgnino@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'dataloader = ffulgor_dataloader.ffulgor_dataloader_node:main'
        ],
    },
)
