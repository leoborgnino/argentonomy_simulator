# Prerequisitos

sudo apt install -y software-properties-common
sudo add-apt-repository universe


sudo apt update && sudo apt install -y curl gnupg lsb-release
sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg


echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null

# Instalar tools necesarias

sudo apt update && sudo apt install -y \
  build-essential \
  cmake \
  git \
  python3-colcon-common-extensions \
  python3-flake8 \
  python3-pip \
  python3-pytest-cov \
  python3-rosdep \
  python3-setuptools \
  python3-vcstool \
  wget
  
# install some pip packages needed for testing
python3 -m pip install -U \
  flake8-blind-except \
  flake8-builtins \
  flake8-class-newline \
  flake8-comprehensions \
  flake8-deprecated \
  flake8-docstrings \
  flake8-import-order \
  flake8-quotes \
  pytest-repeat \
  pytest-rerunfailures \
  pytest \
  setuptools

# Descarga ROS2

cd ~
wget --no-check-certificate --content-disposition https://github.com/ros2/ros2/releases/download/release-galactic-20210716/ros2-galactic-20210616-linux-focal-amd64.tar.bz2
mkdir -p ~/ros2_galactic
cd ~/ros2_galactic
tar xfv ../ros2-galactic-20210616-linux-focal-amd64.tar.bz2

# Instalar dependencias ros2

sudo rosdep init
rosdep update
rosdep install --from-paths src --ignore-src -y --skip-keys "fastcdr rti-connext-dds-5.3.1 urdfdom_headers"

# Descargar paquetes extra
sudo apt install -y ros-galactic-ament-cmake-python
sudo apt install -y libssl-dev
sudo apt install -y python3-opencv
sudo apt install -y libboost-python-dev
sudo apt install -y python3-tqdm
pip3 install transforms3d
pip3 install torch
pip3 install pykitti
